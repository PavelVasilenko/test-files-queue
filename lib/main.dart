import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'model/files_queue.dart';
import 'screens/files/files_screen.dart';
import 'screens/main/main_screen.dart';

void main() {
  GetIt.I.registerSingleton<FilesQueue>(FilesQueue(maxUploadingFilesCount: 3));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  static const String routeHome = '/';
  static const String routeFilesQueue = '/files_queue';

  MyApp({Key key}):super(key:key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Uploading screens.files',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        routeHome: (ctx) => HomeScreen(),
        routeFilesQueue: (ctx) => FilesScreen()
      },
    );
  }
}

