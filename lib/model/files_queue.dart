import 'dart:async';
import 'dart:math';

import '../values/strings.dart';

enum FILE_STATUS { uploaded, uploading, waiting }

class UserFile {
  String fileName;
  FILE_STATUS status = FILE_STATUS.waiting;
  Function(FILE_STATUS) onStateChanged;

  UserFile(this.fileName, this.onStateChanged) : assert (onStateChanged != null);

  String textStatus() {
    switch (status) {
      case FILE_STATUS.uploaded:
        return Strings.fileStatusUploaded;
      case FILE_STATUS.uploading:
        return Strings.fileStatusUploading;
      case FILE_STATUS.waiting:
        return Strings.fileStatusWaiting;
    }
    throw UnimplementedError('Unknown file status');
  }

  void _setUploadStatus(FILE_STATUS newStatus) {
    status = newStatus;
    onStateChanged(status);
  }

  Future<void> upload() async {
    _setUploadStatus(FILE_STATUS.uploading);

    await Future<void>.delayed(Duration(seconds: 1 + Random().nextInt(5)));

    _setUploadStatus(FILE_STATUS.uploaded);
  }
}

class FilesQueue {
  final int maxUploadingFilesCount;
  final List<UserFile> _queue = <UserFile>[];

  FilesQueue({this.maxUploadingFilesCount});

  List<UserFile> get filesQueue => _queue;

  final StreamController<List<UserFile>> _streamController =
      StreamController.broadcast();

  Stream<List<UserFile>> get stream => _streamController.stream;


  void dispose() {
    _streamController.close();
  }

  void addFile(String fileName) {
    _queue.add(UserFile(fileName, _onFileStateChanged));
    _streamController.sink.add(_queue);
    uploadNextFiles();
  }


  void deleteFile(UserFile file) {
    if (_queue.remove(file)) {
      _streamController.sink.add(_queue);
      uploadNextFiles();
    }
  }


  void clear() {
    _queue.clear();
    _streamController.sink.add(_queue);
  }


  void _onFileStateChanged(FILE_STATUS status) {
    _streamController.sink.add(_queue);
    if(status == FILE_STATUS.uploaded) uploadNextFiles();
  }


  Future<void> uploadNextFiles() async {
    int uploadingCount = _queue
        .where((element) => element.status == FILE_STATUS.uploading)
        .length;
    if (uploadingCount >= maxUploadingFilesCount) return;

    _queue
        .where((element) => element.status == FILE_STATUS.waiting)
        .take(maxUploadingFilesCount - uploadingCount)
        .forEach((element) {
      element.upload();
    });
  }
}
