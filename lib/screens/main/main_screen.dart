import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../main.dart';
import '../../values/strings.dart';
import 'bloc/bloc.dart';
import 'bloc/events.dart';
import 'bloc/states.dart';

// -----------------------------------------------------------------------------
class HomeScreen extends StatelessWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeScreenBloc>(
        create: (context) => HomeScreenBloc(StateUninitialized()),
        child: BlocBuilder<HomeScreenBloc, HomeScreenStates>(
          builder: (context, state) {
            if (state is StateUninitialized) {
              BlocProvider.of<HomeScreenBloc>(context).add(EventInitialize());
              return _PendingPage();
            }

            if (state is StateWithQueueData) {
              return _ContentPage(state);
            }

            throw UnimplementedError('Unknown HomePage state');
          },
        ));
  }
}

// -----------------------------------------------------------------------------
class _ContentPage extends StatelessWidget {
  final StateWithQueueData data;

  _ContentPage(this.data, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(Strings.homePageTitle),
          centerTitle: true
        ),
        body: Column(children: [
          InkWell(
              onTap: () => _navigateToFileListScreen(context),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: QueueStatus(data),
              )),
        ]),
        bottomNavigationBar: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FlatButton(
              onPressed: data.resetButtonAvailable
                  ? () => onResetPressed(context)
                  : null,
              child: const Text(Strings.resetButton),
            ),
            FlatButton(
              onPressed: data.saveButtonAvailable
                  ? () => onSavePressed(context)
                  : null,
              child: const Text(Strings.saveButton),
            )
          ],
        ));
  }

  Future<void> _navigateToFileListScreen(BuildContext context) async {
    Navigator.of(context).pushNamed(MyApp.routeFilesQueue);
  }

  void onResetPressed(BuildContext context) {
    BlocProvider.of<HomeScreenBloc>(context).add(EventOnResetPressed());
  }

  Future<void> onSavePressed(BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text(Strings.dialogTitle),
          content: const Text(Strings.dialogText),
          actions: <Widget>[
            FlatButton(
              child: const Text(Strings.dialogButton),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

// -----------------------------------------------------------------------------
class QueueStatus extends StatelessWidget {
  final StateWithQueueData data;

  QueueStatus(this.data, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                Strings.files,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black87),
              ),
              Container(height: 4.0),
              Text(buildSubtitle(
                  data.filesQueueLength, data.notLoadedFilesCount),
              style: const TextStyle(fontSize: 14, color: Colors.black45),),
            ],
          ),
        ),
        const Icon(Icons.chevron_right, color: Colors.black45,)
      ],
    );
  }

  String buildSubtitle(int filesQueueLength, int notLoadedFilesCount) {
    if (filesQueueLength == 0) {
      return Strings.noFiles;
    } else if (notLoadedFilesCount == 0) {
      return '${Strings.filesCount} $filesQueueLength';
    } else {
      return '${Strings.filesLeft} $notLoadedFilesCount. ${Strings.totalFiles} $filesQueueLength';
    }
  }
}

// -----------------------------------------------------------------------------
class _PendingPage extends StatelessWidget {
  _PendingPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(Strings.homePageTitle),
          centerTitle: true
        ),
        body: const Center(
          child: CircularProgressIndicator(),
        ));
  }
}
