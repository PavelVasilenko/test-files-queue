import '../../../model/files_queue.dart';

abstract class HomeScreenEvents {}

class EventInitialize extends HomeScreenEvents{}

class EventOnResetPressed extends HomeScreenEvents{}

class EventOnSavePressed extends HomeScreenEvents{}

class EventUpdateData extends HomeScreenEvents{
  final List<UserFile> files;
  EventUpdateData(this.files);
}