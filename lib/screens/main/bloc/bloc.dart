import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import '../../../model/files_queue.dart';
import 'events.dart';
import 'states.dart';

class HomeScreenBloc extends Bloc<HomeScreenEvents, HomeScreenStates> {
  HomeScreenBloc(HomeScreenStates initialState) : super(initialState);

  StreamSubscription<List<UserFile>> _queueSubscription;

  @override
  Future<void> close() {
    _queueSubscription?.cancel();
    GetIt.I<FilesQueue>().dispose();
    return super.close();
  }

  @override
  Stream<HomeScreenStates> mapEventToState(HomeScreenEvents event) async* {
    if (event is EventInitialize) {
      yield* mapOnInitializeToState();
    } else if(event is EventUpdateData) {
      yield* mapUpdateDataToState(event);
    } else if(event is EventOnResetPressed) {
      yield* mapResetPressedToState();
    }

  }

  Stream<HomeScreenStates> mapResetPressedToState() async* {
    GetIt.I<FilesQueue>().clear();
  }

  Stream<HomeScreenStates> mapOnInitializeToState() async* {
    FilesQueue _queue = GetIt.I<FilesQueue>();

    _queueSubscription = _queue.stream.listen(_queueListener);
    add(EventUpdateData(_queue.filesQueue));
  }

  Stream<HomeScreenStates> mapUpdateDataToState(EventUpdateData event) async* {

    final int _filesQueueLength = event.files?.length ?? 0;
    final int _notLoadedFilesCount = event.files
        ?.where((element) => element.status != FILE_STATUS.uploaded)
        ?.length ??
        0;
    
    yield StateWithQueueData(
        filesQueueLength: _filesQueueLength,
        notLoadedFilesCount: _notLoadedFilesCount,
        resetButtonAvailable: _filesQueueLength > 0,
        saveButtonAvailable:
        _filesQueueLength != 0 || _notLoadedFilesCount > 0);
  }

  void _queueListener(List<UserFile> data) {
    add(EventUpdateData(data));
  }
}
