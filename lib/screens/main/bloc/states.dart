import 'package:flutter/material.dart';

abstract class HomeScreenStates {}

class StateUninitialized extends HomeScreenStates {}

class StateWithQueueData extends HomeScreenStates {
  final int filesQueueLength;
  final int notLoadedFilesCount;
  final bool resetButtonAvailable;
  final bool saveButtonAvailable;

  StateWithQueueData({
      @required this.filesQueueLength,
      @required this.notLoadedFilesCount,
      this.resetButtonAvailable = false,
      this.saveButtonAvailable = false})
      : assert(filesQueueLength != null, notLoadedFilesCount != null);
}
