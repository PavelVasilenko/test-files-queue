import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../model/files_queue.dart';
import '../../values/strings.dart';
import 'bloc/bloc.dart';
import 'bloc/events.dart';
import 'bloc/states.dart';

// -----------------------------------------------------------------------------
class FilesScreen extends StatelessWidget {
  FilesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FilesScreenBloc>(
        create: (context) => FilesScreenBloc(StateUninitialized()),
        child: BlocBuilder<FilesScreenBloc, FilesScreenStates>(
          builder: (context, state) {
            if (state is StateUninitialized) {
              BlocProvider.of<FilesScreenBloc>(context).add(EventInitialize());
              return _PendingPage();
            }

            if (state is StateFilesList) {
              return ((state.files?.length ?? 0) == 0)
                  ? _NoFilesPage()
                  : _ContentPage(state);
            }

            throw UnimplementedError('Unknown FilesScreen state');
          },
        ));
  }
}

// -----------------------------------------------------------------------------
class _ContentPage extends StatelessWidget {
  final StateFilesList data;

  _ContentPage(this.data, {Key key})
      : assert(data != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text(Strings.filesPageTitle), centerTitle: true),
        body: ListView.builder(
          itemBuilder: (context, index) {
            return FileStatus(data.files[index]);
          },
          itemCount: data.files?.length ?? 0,
        ),
        floatingActionButton: Visibility(
          visible: (data.files?.length ?? 0) < 30,
          child: FloatingActionButton(
              child: const Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () => onAddPressed(context)),
        ));
  }

  void onAddPressed(BuildContext context) {
    BlocProvider.of<FilesScreenBloc>(context).add(EventOnAddFilePressed());
  }
}

// -----------------------------------------------------------------------------
class _NoFilesPage extends StatelessWidget {
  _NoFilesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          AppBar(title: const Text(Strings.filesPageTitle), centerTitle: true),
      body: const Center(
        child: Text(Strings.emptyList),
      ),
      floatingActionButton: FloatingActionButton(
          child: const Icon(
            Icons.add,
            color: Colors.white,
          ),
          onPressed: () => onAddPressed(context)),
    );
  }

  void onAddPressed(BuildContext context) {
    BlocProvider.of<FilesScreenBloc>(context).add(EventOnAddFilePressed());
  }
}

// -----------------------------------------------------------------------------
class FileStatus extends StatelessWidget {
  final UserFile userFile;

  FileStatus(this.userFile, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  userFile.fileName,
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87),
                ),
                Container(height: 4.0),
                Text(
                  userFile.textStatus(),
                  style: const TextStyle(fontSize: 14, color: Colors.black45),
                ),
              ],
            ),
          ),
          IconButton(
            icon: const Icon(
              Icons.clear,
              color: Colors.black45,
            ),
            onPressed: () {
              BlocProvider.of<FilesScreenBloc>(context)
                  .add(EventOnDeletePressed(userFile));
            },
          )
        ],
      ),
    );
  }
}

// -----------------------------------------------------------------------------
class _PendingPage extends StatelessWidget {
  _PendingPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text(Strings.filesPageTitle), centerTitle: true),
        body: const Center(
          child: CircularProgressIndicator(),
        ));
  }
}
