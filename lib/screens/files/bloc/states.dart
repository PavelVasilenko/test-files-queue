import 'package:flutter/material.dart';
import '../../../model/files_queue.dart';

abstract class FilesScreenStates {}

class StateUninitialized extends FilesScreenStates {}

class StateFilesList extends FilesScreenStates {
  final List<UserFile> files;

  StateFilesList({@required this.files,})
      : assert(files != null);
}
