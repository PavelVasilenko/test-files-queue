import '../../../model/files_queue.dart';

abstract class FilesScreenEvents {}

class EventInitialize extends FilesScreenEvents{}

class EventUpdateQueue extends FilesScreenEvents{
  final List<UserFile> files;
  EventUpdateQueue(this.files);
}

class EventOnAddFilePressed extends FilesScreenEvents{}

class EventOnDeletePressed extends FilesScreenEvents{
  final UserFile file;
  EventOnDeletePressed(this.file);
}