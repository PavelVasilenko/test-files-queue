import 'dart:async';
import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import '../../../model/files_queue.dart';
import '../../../values/strings.dart';
import 'events.dart';
import 'states.dart';

class FilesScreenBloc extends Bloc<FilesScreenEvents, FilesScreenStates> {

  FilesScreenBloc(FilesScreenStates initialState) : super(initialState);

  StreamSubscription< List<UserFile>> _queueSubscription;

  @override
  Future<void> close() {
    _queueSubscription?.cancel();
    return super.close();
  }

  @override
  Stream<FilesScreenStates> mapEventToState(FilesScreenEvents event) async* {
    if (event is EventInitialize) {
      yield* mapInitializeToState();
    } else if (event is EventOnAddFilePressed) {
      yield* mapAddFileToState();
    } else if (event is EventOnDeletePressed) {
      yield* mapDeleteFileToState(event);
    } else if(event is EventUpdateQueue) {
      yield StateFilesList(files:event.files);
    }
  }

  Stream<FilesScreenStates> mapInitializeToState() async* {
    _queueSubscription = GetIt.I<FilesQueue>().stream.listen(_queueListener);
    yield StateFilesList(files: GetIt.I<FilesQueue>().filesQueue);
  }

  Stream<FilesScreenStates> mapAddFileToState() async* {
    int fileNumber = 1000 + Random().nextInt(8999);
    GetIt.I<FilesQueue>().addFile('${Strings.fileName} #$fileNumber');
  }

  Stream<FilesScreenStates> mapDeleteFileToState(
      EventOnDeletePressed event) async* {
    GetIt.I<FilesQueue>().deleteFile(event.file);
  }

  void _queueListener(List<UserFile> data) {
    add(EventUpdateQueue(data));
  }
}
