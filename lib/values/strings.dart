class Strings{
  static const String homePageTitle = 'Home page';
  static const String resetButton = 'Сбросить';
  static const String saveButton = 'Сохранить';
  static const String files = 'Файлы';
  static const String noFiles = 'Нет файлов';
  static const String filesCount = 'Кол-во файлов:';
  static const String filesLeft = 'Осталось загрузить:';
  static const String totalFiles = 'Всего файлов:';

  static const String filesPageTitle = 'Файлы';
  static const String emptyList = 'Нет файлов';
  static const String fileStatusUploaded = '';
  static const String fileStatusUploading = 'Загружается';
  static const String fileStatusWaiting = 'В ожидании';

  static const String fileName = 'Файл';

  static const String dialogTitle = 'Выполнено';
  static const String dialogText = 'Файлы успешно сохранены';
  static const String dialogButton = 'OK';
}